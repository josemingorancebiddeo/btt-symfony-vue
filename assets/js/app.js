import Vue from 'vue';
import App from './components/App';
import Routes from './routes';
import '../styles/app.css';

new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App)
});