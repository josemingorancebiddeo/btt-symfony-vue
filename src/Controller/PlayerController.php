<?php

namespace App\Controller;

use App\Entity\Player;
use App\Entity\Team;
use App\EntityManager\PlayerManager;
use App\EntityManager\TeamManager;
use App\Repository\PlayerRepository;
use App\Repository\TeamRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;

class PlayerController extends AbstractController
{
    /** @var PlayerManager $playerManager */
    private $playerManager;

    /** @var TeamManager $teamManager */
    private $teamManager;

    /** @var UrlGeneratorInterface $urlGenerator */
    private $urlGenerator;

    public function __construct(PlayerManager         $playerManager,
                                TeamManager           $teamManager,
                                UrlGeneratorInterface $urlGenerator)
    {
        $this->playerManager = $playerManager;
        $this->teamManager = $teamManager;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/player/", name="player")
     */
    public function indexView(): Response
    {
        return $this->render('home/index.html.twig', []);
    }

    /**
     * @Route("/player/detail/", name="player_detail")
     */
    public function detailView(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/player/edit/", name="player_edit")
     */
    public function editView(Request $request): RedirectResponse
    {
        // TODO
    }

    /**
     * @Route("/player/list/", name="player_list")
     */
    public function list(): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/player/create/", name="player_create", methods={"POST"})
     * @throws ORMException
     */
    public function create(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/player/update/", name="player_update", methods={"POST"})
     * @throws ORMException
     */
    public function update(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/player/delete/", name="player_delete", methods={"POST"})
     * @throws ORMException
     */
    public function delete(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/player/random/", name="player_delete")
     */
    public function random(Request $request): JsonResponse
    {
        // TODO
    }
}