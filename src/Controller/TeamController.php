<?php

namespace App\Controller;

use App\Entity\Team;
use App\EntityManager\PlayerManager;
use App\EntityManager\TeamManager;
use App\Repository\TeamRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TeamController extends AbstractController
{
    /** @var TeamManager $teamManager */
    private $teamManager;

    /** @var UrlGeneratorInterface $urlGenerator */
    private $urlGenerator;

    public function __construct(TeamManager           $teamManager,
                                UrlGeneratorInterface $urlGenerator)
    {
        $this->teamManager = $teamManager;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/team/", name="team")
     */
    public function indexView(): Response
    {
        return $this->render('home/index.html.twig', []);
    }

    /**
     * @Route("/team/detail/", name="team_detail")
     */
    public function detailView(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/team/edit/", name="team_edit")
     */
    public function editView(Request $request): RedirectResponse
    {
        // TODO
    }

    /**
     * @Route("/team/list/", name="team_list")
     */
    public function list(): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/team/create/", name="team_create", methods={"POST"})
     * @throws ORMException
     */
    public function create(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/team/update/", name="team_update", methods={"POST"})
     * @throws ORMException
     */
    public function update(Request $request): JsonResponse
    {
        // TODO
    }

    /**
     * @Route("/team/delete/", name="team_delete", methods={"POST"})
     * @throws ORMException
     */
    public function delete(Request $request): JsonResponse
    {
        // TODO
    }
}